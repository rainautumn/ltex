clc
clear

A = [0 0; 0 0];

U_in = 100;
f = 500;

R = 30;
C1 = 10 * 10^-6;
C2 = 10 * 10^-6;

Cx1 = 1 / (2 * pi() * C1 * f) * -j;
Cx2 = 1 / (2 * pi() * C2 * f) * -j;

I_in = U_in /(Cx1 + R)
U_out = I_in * R

atan(imag(I_in)/real(I_in)) * 180 / pi()
atan(imag(U_out)/real(U_out)) * 180 / pi()

abs(I_in) * sqrt(2)
abs(U_out) * sqrt(2)

A(1,1) = U_in/U_out;
A(2,1) = I_in/U_out;

b = [100;0];
a = [Cx1 + R, - R;
     -R     , R + Cx2];

x = inv(a) * b;

I_in = x(1);
I_out = x(2);

A(1,2) = U_in/I_out;
A(2,2) = I_in/I_out;

A

det(A)

Zc = A(2,1)\1

b = [100;0];
a = [Cx1 + R, - R;
     -R     , R + Cx2 + 30];

x = inv(a) * b;

I_in = x(1)
I_out = x(2)
U_out = I_out * 30

180 + atan(imag(I_out)/real(I_out)) * 180 / pi()
atan(imag(I_in)/real(I_in)) * 180 / pi()
180 + atan(imag(U_out)/real(U_out)) * 180 / pi()

abs(I_out) * sqrt(2)
abs(I_in) * sqrt(2)
abs(U_out) * sqrt(2)