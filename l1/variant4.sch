<Qucs Schematic 0.0.18>
<Properties>
  <View=0,-60,917,800,0.751315,0,0>
  <Grid=10,10,1>
  <DataSet=variant4.dat>
  <DataDisplay=variant4.dpl>
  <OpenDisplay=1>
  <Script=variant4.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Vac V1 1 140 230 18 -26 0 1 "100 V" 1 "1 GHz" 0 "0" 0 "0" 0>
  <GND * 1 840 160 0 0 0 0>
  <R R1 1 450 240 15 -26 0 1 "30 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <C C1 1 380 160 -26 17 0 0 "10 uF" 1 "" 0 "neutral" 0>
  <C C2 1 520 160 -26 17 0 0 "10 uF" 1 "" 0 "neutral" 0>
  <GND * 1 450 350 0 0 0 0>
  <.AC AC1 1 30 20 0 43 0 0 "const" 1 "1 GHz" 0 "10 GHz" 0 "[500 Hz;]" 1 "no" 0>
  <IProbe I_in 1 270 320 -26 16 1 2>
  <VProbe Uout 1 830 140 28 -31 0 0>
  <IProbe I_out 1 710 320 -26 16 1 2>
  <R R2 1 790 220 15 -26 0 1 "30 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
</Components>
<Wires>
  <450 160 450 210 "" 0 0 0 "">
  <790 160 820 160 "" 0 0 0 "">
  <790 160 790 190 "" 0 0 0 "">
  <410 160 450 160 "" 0 0 0 "">
  <140 160 140 200 "" 0 0 0 "">
  <140 160 350 160 "" 0 0 0 "">
  <550 160 790 160 "" 0 0 0 "">
  <450 160 490 160 "" 0 0 0 "">
  <450 270 450 320 "" 0 0 0 "">
  <450 320 450 350 "" 0 0 0 "">
  <300 320 450 320 "" 0 0 0 "">
  <140 260 140 320 "" 0 0 0 "">
  <140 320 240 320 "" 0 0 0 "">
  <740 320 790 320 "" 0 0 0 "">
  <790 250 790 320 "" 0 0 0 "">
  <450 320 680 320 "" 0 0 0 "">
</Wires>
<Diagrams>
  <Tab 180 71 481 51 3 #c0c0c0 1 00 1 0 1 1 1 0 1 1 1 0 1 1 315 0 225 "" "" "">
	<"Uout.v" #0000ff 0 3 1 0 0>
	<"I_in.i" #0000ff 0 3 1 0 0>
	<"I_out.i" #0000ff 0 3 1 0 0>
  </Tab>
</Diagrams>
<Paintings>
</Paintings>
