<Qucs Schematic 0.0.18>
<Properties>
  <View=0,-120,1304,845,0.751315,0,0>
  <Grid=10,10,1>
  <DataSet=sh1.dat>
  <DataDisplay=sh1.dpl>
  <OpenDisplay=1>
  <Script=sh1.m>
  <RunScript=0>
  <showFrame=0>
  <FrameText0=Title>
  <FrameText1=Drawn By:>
  <FrameText2=Date:>
  <FrameText3=Revision:>
</Properties>
<Symbol>
</Symbol>
<Components>
  <Vdc V1 1 210 160 18 -26 0 1 "100 V" 1>
  <GND * 1 210 190 0 0 0 0>
  <L L1 1 650 120 10 -26 0 1 "500 uH" 1 "" 0>
  <GND * 1 740 180 0 0 0 0>
  <GND * 1 650 180 0 0 0 0>
  <VProbe U_kaduzki 1 900 -20 28 -31 0 0>
  <GND * 1 910 0 0 0 0 0>
  <R R3 1 740 120 15 -26 0 1 "10 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <R R4 1 470 10 -26 -43 0 2 "25 Ohm" 1 "26.85" 0 "0.0" 0 "0.0" 0 "26.85" 0 "US" 0>
  <IProbe I_katuzki 1 650 50 -13 -26 1 3>
  <Switch S3 1 360 10 -26 11 0 0 "on" 0 "2 ms" 1 "0" 0 "1e12" 0 "26.85" 0 "1e-6" 0>
  <Switch S2 1 260 10 -26 11 0 0 "off" 0 "1 ms" 1 "0" 0 "1e12" 0 "26.85" 0 "1e-6" 0>
  <.TR TR1 1 1110 0 0 70 0 0 "lin" 1 "0 ms" 1 "4 ms" 1 "4000" 0 "Trapezoidal" 0 "2" 0 "1 ns" 0 "1e-16" 0 "150" 0 "0.001" 0 "1 pA" 0 "1 uV" 0 "26.85" 0 "1e-3" 0 "1e-6" 0 "1" 0 "CroutLU" 0 "no" 0 "yes" 0 "0" 0>
</Components>
<Wires>
  <650 150 650 180 "" 0 0 0 "">
  <740 150 740 180 "" 0 0 0 "">
  <890 0 890 10 "" 0 0 0 "">
  <740 10 890 10 "" 0 0 0 "">
  <740 10 740 90 "" 0 0 0 "">
  <210 10 210 130 "" 0 0 0 "">
  <210 10 230 10 "" 0 0 0 "">
  <390 10 440 10 "" 0 0 0 "">
  <290 10 330 10 "" 0 0 0 "">
  <650 80 650 90 "" 0 0 0 "">
  <500 10 650 10 "" 0 0 0 "">
  <650 10 740 10 "" 0 0 0 "">
  <650 10 650 20 "" 0 0 0 "">
</Wires>
<Diagrams>
</Diagrams>
<Paintings>
</Paintings>
